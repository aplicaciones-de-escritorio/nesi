﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NESI
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(NESI))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ErCuatrimestreToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ErCuatrimestreToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DesarrolloHumanoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExpresiónOralYEscritaIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FormaciónSocioculturalIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FundamentosPedagógicosEnLaEducaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformáticaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InglésIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MatemáticasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DoCuatrimestreToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DiseñoDeMaterialDidácticoIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstadísticaAplicadaALaEducaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExpresiónOralYEscritaIIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InglésIIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MetodologíaDeLaDidácticaIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MetodologíaDeLaInvestigaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RoCuatrimestreToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EvaluaciónDelProcesoEnseñanzaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FormaciónSocioculturalIIIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InglésIIIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaEducaciónEnMéxicoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MetodologíaDeLaDidácticaIIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MetodologíaDeLaInvestigaciónToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlaneaciónDocenteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToCuatrimestreToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DiseñoDeSituacionesDeEnseñanzaYAprendizajeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstrategiasDeEnseñanzaEnLaEducaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExpresiónOralYEscritaIIToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.FonéticaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FormaciónSocioculturalIVToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InstrumentosDeEvaluaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IntegradoraIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlaneaciónDocenteToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToCuatrmestreToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContabilidadDeCostosIIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EnseñanzaDeHabilidadesProductivasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EnseñanzaDeHabilidadesReceptivasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstrategiasDeEnseñanzaEnLaLenguaInglesaIIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstructuraGramáticalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InglesVToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IntegradoraIIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label4 = New System.Windows.Forms.Label()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        resources.ApplyResources(Me.PictureBox1, "PictureBox1")
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.TabStop = False
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Name = "Label1"
        '
        'MenuStrip1
        '
        resources.ApplyResources(Me.MenuStrip1, "MenuStrip1")
        Me.MenuStrip1.BackColor = System.Drawing.Color.LightSeaGreen
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ErCuatrimestreToolStripMenuItem})
        Me.MenuStrip1.Name = "MenuStrip1"
        '
        'ErCuatrimestreToolStripMenuItem
        '
        Me.ErCuatrimestreToolStripMenuItem.BackColor = System.Drawing.Color.MintCream
        Me.ErCuatrimestreToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ErCuatrimestreToolStripMenuItem1, Me.DoCuatrimestreToolStripMenuItem, Me.RoCuatrimestreToolStripMenuItem, Me.ToCuatrimestreToolStripMenuItem, Me.ToCuatrmestreToolStripMenuItem})
        Me.ErCuatrimestreToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.ErCuatrimestreToolStripMenuItem.Name = "ErCuatrimestreToolStripMenuItem"
        resources.ApplyResources(Me.ErCuatrimestreToolStripMenuItem, "ErCuatrimestreToolStripMenuItem")
        '
        'ErCuatrimestreToolStripMenuItem1
        '
        Me.ErCuatrimestreToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DesarrolloHumanoToolStripMenuItem, Me.ExpresiónOralYEscritaIToolStripMenuItem, Me.FormaciónSocioculturalIToolStripMenuItem, Me.FundamentosPedagógicosEnLaEducaciónToolStripMenuItem, Me.InformáticaToolStripMenuItem, Me.InglésIToolStripMenuItem, Me.MatemáticasToolStripMenuItem})
        Me.ErCuatrimestreToolStripMenuItem1.ForeColor = System.Drawing.Color.DarkCyan
        Me.ErCuatrimestreToolStripMenuItem1.Name = "ErCuatrimestreToolStripMenuItem1"
        resources.ApplyResources(Me.ErCuatrimestreToolStripMenuItem1, "ErCuatrimestreToolStripMenuItem1")
        '
        'DesarrolloHumanoToolStripMenuItem
        '
        Me.DesarrolloHumanoToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.DesarrolloHumanoToolStripMenuItem.Name = "DesarrolloHumanoToolStripMenuItem"
        resources.ApplyResources(Me.DesarrolloHumanoToolStripMenuItem, "DesarrolloHumanoToolStripMenuItem")
        '
        'ExpresiónOralYEscritaIToolStripMenuItem
        '
        Me.ExpresiónOralYEscritaIToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.ExpresiónOralYEscritaIToolStripMenuItem.Name = "ExpresiónOralYEscritaIToolStripMenuItem"
        resources.ApplyResources(Me.ExpresiónOralYEscritaIToolStripMenuItem, "ExpresiónOralYEscritaIToolStripMenuItem")
        '
        'FormaciónSocioculturalIToolStripMenuItem
        '
        Me.FormaciónSocioculturalIToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.FormaciónSocioculturalIToolStripMenuItem.Name = "FormaciónSocioculturalIToolStripMenuItem"
        resources.ApplyResources(Me.FormaciónSocioculturalIToolStripMenuItem, "FormaciónSocioculturalIToolStripMenuItem")
        '
        'FundamentosPedagógicosEnLaEducaciónToolStripMenuItem
        '
        Me.FundamentosPedagógicosEnLaEducaciónToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.FundamentosPedagógicosEnLaEducaciónToolStripMenuItem.Name = "FundamentosPedagógicosEnLaEducaciónToolStripMenuItem"
        resources.ApplyResources(Me.FundamentosPedagógicosEnLaEducaciónToolStripMenuItem, "FundamentosPedagógicosEnLaEducaciónToolStripMenuItem")
        '
        'InformáticaToolStripMenuItem
        '
        Me.InformáticaToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.InformáticaToolStripMenuItem.Name = "InformáticaToolStripMenuItem"
        resources.ApplyResources(Me.InformáticaToolStripMenuItem, "InformáticaToolStripMenuItem")
        '
        'InglésIToolStripMenuItem
        '
        Me.InglésIToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.InglésIToolStripMenuItem.Name = "InglésIToolStripMenuItem"
        resources.ApplyResources(Me.InglésIToolStripMenuItem, "InglésIToolStripMenuItem")
        '
        'MatemáticasToolStripMenuItem
        '
        Me.MatemáticasToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.MatemáticasToolStripMenuItem.Name = "MatemáticasToolStripMenuItem"
        resources.ApplyResources(Me.MatemáticasToolStripMenuItem, "MatemáticasToolStripMenuItem")
        '
        'DoCuatrimestreToolStripMenuItem
        '
        Me.DoCuatrimestreToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DiseñoDeMaterialDidácticoIToolStripMenuItem, Me.EstadísticaAplicadaALaEducaciónToolStripMenuItem, Me.ExpresiónOralYEscritaIIToolStripMenuItem, Me.InglésIIToolStripMenuItem, Me.MetodologíaDeLaDidácticaIToolStripMenuItem, Me.MetodologíaDeLaInvestigaciónToolStripMenuItem})
        Me.DoCuatrimestreToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.DoCuatrimestreToolStripMenuItem.Name = "DoCuatrimestreToolStripMenuItem"
        resources.ApplyResources(Me.DoCuatrimestreToolStripMenuItem, "DoCuatrimestreToolStripMenuItem")
        '
        'DiseñoDeMaterialDidácticoIToolStripMenuItem
        '
        Me.DiseñoDeMaterialDidácticoIToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.DiseñoDeMaterialDidácticoIToolStripMenuItem.Name = "DiseñoDeMaterialDidácticoIToolStripMenuItem"
        resources.ApplyResources(Me.DiseñoDeMaterialDidácticoIToolStripMenuItem, "DiseñoDeMaterialDidácticoIToolStripMenuItem")
        '
        'EstadísticaAplicadaALaEducaciónToolStripMenuItem
        '
        Me.EstadísticaAplicadaALaEducaciónToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.EstadísticaAplicadaALaEducaciónToolStripMenuItem.Name = "EstadísticaAplicadaALaEducaciónToolStripMenuItem"
        resources.ApplyResources(Me.EstadísticaAplicadaALaEducaciónToolStripMenuItem, "EstadísticaAplicadaALaEducaciónToolStripMenuItem")
        '
        'ExpresiónOralYEscritaIIToolStripMenuItem
        '
        Me.ExpresiónOralYEscritaIIToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.ExpresiónOralYEscritaIIToolStripMenuItem.Name = "ExpresiónOralYEscritaIIToolStripMenuItem"
        resources.ApplyResources(Me.ExpresiónOralYEscritaIIToolStripMenuItem, "ExpresiónOralYEscritaIIToolStripMenuItem")
        '
        'InglésIIToolStripMenuItem
        '
        Me.InglésIIToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.InglésIIToolStripMenuItem.Name = "InglésIIToolStripMenuItem"
        resources.ApplyResources(Me.InglésIIToolStripMenuItem, "InglésIIToolStripMenuItem")
        '
        'MetodologíaDeLaDidácticaIToolStripMenuItem
        '
        Me.MetodologíaDeLaDidácticaIToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.MetodologíaDeLaDidácticaIToolStripMenuItem.Name = "MetodologíaDeLaDidácticaIToolStripMenuItem"
        resources.ApplyResources(Me.MetodologíaDeLaDidácticaIToolStripMenuItem, "MetodologíaDeLaDidácticaIToolStripMenuItem")
        '
        'MetodologíaDeLaInvestigaciónToolStripMenuItem
        '
        Me.MetodologíaDeLaInvestigaciónToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.MetodologíaDeLaInvestigaciónToolStripMenuItem.Name = "MetodologíaDeLaInvestigaciónToolStripMenuItem"
        resources.ApplyResources(Me.MetodologíaDeLaInvestigaciónToolStripMenuItem, "MetodologíaDeLaInvestigaciónToolStripMenuItem")
        '
        'RoCuatrimestreToolStripMenuItem
        '
        Me.RoCuatrimestreToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EvaluaciónDelProcesoEnseñanzaToolStripMenuItem, Me.FormaciónSocioculturalIIIToolStripMenuItem, Me.InglésIIIToolStripMenuItem, Me.LaEducaciónEnMéxicoToolStripMenuItem, Me.MetodologíaDeLaDidácticaIIToolStripMenuItem, Me.MetodologíaDeLaInvestigaciónToolStripMenuItem1, Me.PlaneaciónDocenteToolStripMenuItem})
        Me.RoCuatrimestreToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.RoCuatrimestreToolStripMenuItem.Name = "RoCuatrimestreToolStripMenuItem"
        resources.ApplyResources(Me.RoCuatrimestreToolStripMenuItem, "RoCuatrimestreToolStripMenuItem")
        '
        'EvaluaciónDelProcesoEnseñanzaToolStripMenuItem
        '
        Me.EvaluaciónDelProcesoEnseñanzaToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.EvaluaciónDelProcesoEnseñanzaToolStripMenuItem.Name = "EvaluaciónDelProcesoEnseñanzaToolStripMenuItem"
        resources.ApplyResources(Me.EvaluaciónDelProcesoEnseñanzaToolStripMenuItem, "EvaluaciónDelProcesoEnseñanzaToolStripMenuItem")
        '
        'FormaciónSocioculturalIIIToolStripMenuItem
        '
        Me.FormaciónSocioculturalIIIToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.FormaciónSocioculturalIIIToolStripMenuItem.Name = "FormaciónSocioculturalIIIToolStripMenuItem"
        resources.ApplyResources(Me.FormaciónSocioculturalIIIToolStripMenuItem, "FormaciónSocioculturalIIIToolStripMenuItem")
        '
        'InglésIIIToolStripMenuItem
        '
        Me.InglésIIIToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.InglésIIIToolStripMenuItem.Name = "InglésIIIToolStripMenuItem"
        resources.ApplyResources(Me.InglésIIIToolStripMenuItem, "InglésIIIToolStripMenuItem")
        '
        'LaEducaciónEnMéxicoToolStripMenuItem
        '
        Me.LaEducaciónEnMéxicoToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.LaEducaciónEnMéxicoToolStripMenuItem.Name = "LaEducaciónEnMéxicoToolStripMenuItem"
        resources.ApplyResources(Me.LaEducaciónEnMéxicoToolStripMenuItem, "LaEducaciónEnMéxicoToolStripMenuItem")
        '
        'MetodologíaDeLaDidácticaIIToolStripMenuItem
        '
        Me.MetodologíaDeLaDidácticaIIToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.MetodologíaDeLaDidácticaIIToolStripMenuItem.Name = "MetodologíaDeLaDidácticaIIToolStripMenuItem"
        resources.ApplyResources(Me.MetodologíaDeLaDidácticaIIToolStripMenuItem, "MetodologíaDeLaDidácticaIIToolStripMenuItem")
        '
        'MetodologíaDeLaInvestigaciónToolStripMenuItem1
        '
        Me.MetodologíaDeLaInvestigaciónToolStripMenuItem1.ForeColor = System.Drawing.Color.DarkCyan
        Me.MetodologíaDeLaInvestigaciónToolStripMenuItem1.Name = "MetodologíaDeLaInvestigaciónToolStripMenuItem1"
        resources.ApplyResources(Me.MetodologíaDeLaInvestigaciónToolStripMenuItem1, "MetodologíaDeLaInvestigaciónToolStripMenuItem1")
        '
        'PlaneaciónDocenteToolStripMenuItem
        '
        Me.PlaneaciónDocenteToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.PlaneaciónDocenteToolStripMenuItem.Name = "PlaneaciónDocenteToolStripMenuItem"
        resources.ApplyResources(Me.PlaneaciónDocenteToolStripMenuItem, "PlaneaciónDocenteToolStripMenuItem")
        '
        'ToCuatrimestreToolStripMenuItem
        '
        Me.ToCuatrimestreToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DiseñoDeSituacionesDeEnseñanzaYAprendizajeToolStripMenuItem, Me.EstrategiasDeEnseñanzaEnLaEducaciónToolStripMenuItem, Me.ExpresiónOralYEscritaIIToolStripMenuItem1, Me.FonéticaToolStripMenuItem, Me.FormaciónSocioculturalIVToolStripMenuItem, Me.InstrumentosDeEvaluaciónToolStripMenuItem, Me.IntegradoraIToolStripMenuItem, Me.PlaneaciónDocenteToolStripMenuItem1})
        Me.ToCuatrimestreToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.ToCuatrimestreToolStripMenuItem.Name = "ToCuatrimestreToolStripMenuItem"
        resources.ApplyResources(Me.ToCuatrimestreToolStripMenuItem, "ToCuatrimestreToolStripMenuItem")
        '
        'DiseñoDeSituacionesDeEnseñanzaYAprendizajeToolStripMenuItem
        '
        Me.DiseñoDeSituacionesDeEnseñanzaYAprendizajeToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.DiseñoDeSituacionesDeEnseñanzaYAprendizajeToolStripMenuItem.Name = "DiseñoDeSituacionesDeEnseñanzaYAprendizajeToolStripMenuItem"
        resources.ApplyResources(Me.DiseñoDeSituacionesDeEnseñanzaYAprendizajeToolStripMenuItem, "DiseñoDeSituacionesDeEnseñanzaYAprendizajeToolStripMenuItem")
        '
        'EstrategiasDeEnseñanzaEnLaEducaciónToolStripMenuItem
        '
        Me.EstrategiasDeEnseñanzaEnLaEducaciónToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.EstrategiasDeEnseñanzaEnLaEducaciónToolStripMenuItem.Name = "EstrategiasDeEnseñanzaEnLaEducaciónToolStripMenuItem"
        resources.ApplyResources(Me.EstrategiasDeEnseñanzaEnLaEducaciónToolStripMenuItem, "EstrategiasDeEnseñanzaEnLaEducaciónToolStripMenuItem")
        '
        'ExpresiónOralYEscritaIIToolStripMenuItem1
        '
        Me.ExpresiónOralYEscritaIIToolStripMenuItem1.ForeColor = System.Drawing.Color.DarkCyan
        Me.ExpresiónOralYEscritaIIToolStripMenuItem1.Name = "ExpresiónOralYEscritaIIToolStripMenuItem1"
        resources.ApplyResources(Me.ExpresiónOralYEscritaIIToolStripMenuItem1, "ExpresiónOralYEscritaIIToolStripMenuItem1")
        '
        'FonéticaToolStripMenuItem
        '
        Me.FonéticaToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.FonéticaToolStripMenuItem.Name = "FonéticaToolStripMenuItem"
        resources.ApplyResources(Me.FonéticaToolStripMenuItem, "FonéticaToolStripMenuItem")
        '
        'FormaciónSocioculturalIVToolStripMenuItem
        '
        Me.FormaciónSocioculturalIVToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.FormaciónSocioculturalIVToolStripMenuItem.Name = "FormaciónSocioculturalIVToolStripMenuItem"
        resources.ApplyResources(Me.FormaciónSocioculturalIVToolStripMenuItem, "FormaciónSocioculturalIVToolStripMenuItem")
        '
        'InstrumentosDeEvaluaciónToolStripMenuItem
        '
        Me.InstrumentosDeEvaluaciónToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.InstrumentosDeEvaluaciónToolStripMenuItem.Name = "InstrumentosDeEvaluaciónToolStripMenuItem"
        resources.ApplyResources(Me.InstrumentosDeEvaluaciónToolStripMenuItem, "InstrumentosDeEvaluaciónToolStripMenuItem")
        '
        'IntegradoraIToolStripMenuItem
        '
        Me.IntegradoraIToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.IntegradoraIToolStripMenuItem.Name = "IntegradoraIToolStripMenuItem"
        resources.ApplyResources(Me.IntegradoraIToolStripMenuItem, "IntegradoraIToolStripMenuItem")
        '
        'PlaneaciónDocenteToolStripMenuItem1
        '
        Me.PlaneaciónDocenteToolStripMenuItem1.ForeColor = System.Drawing.Color.DarkCyan
        Me.PlaneaciónDocenteToolStripMenuItem1.Name = "PlaneaciónDocenteToolStripMenuItem1"
        resources.ApplyResources(Me.PlaneaciónDocenteToolStripMenuItem1, "PlaneaciónDocenteToolStripMenuItem1")
        '
        'ToCuatrmestreToolStripMenuItem
        '
        Me.ToCuatrmestreToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ContabilidadDeCostosIIToolStripMenuItem, Me.EnseñanzaDeHabilidadesProductivasToolStripMenuItem, Me.EnseñanzaDeHabilidadesReceptivasToolStripMenuItem, Me.EstrategiasDeEnseñanzaEnLaLenguaInglesaIIToolStripMenuItem, Me.EstructuraGramáticalToolStripMenuItem, Me.InglesVToolStripMenuItem, Me.IntegradoraIIToolStripMenuItem})
        Me.ToCuatrmestreToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.ToCuatrmestreToolStripMenuItem.Name = "ToCuatrmestreToolStripMenuItem"
        resources.ApplyResources(Me.ToCuatrmestreToolStripMenuItem, "ToCuatrmestreToolStripMenuItem")
        '
        'ContabilidadDeCostosIIToolStripMenuItem
        '
        Me.ContabilidadDeCostosIIToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.ContabilidadDeCostosIIToolStripMenuItem.Name = "ContabilidadDeCostosIIToolStripMenuItem"
        resources.ApplyResources(Me.ContabilidadDeCostosIIToolStripMenuItem, "ContabilidadDeCostosIIToolStripMenuItem")
        '
        'EnseñanzaDeHabilidadesProductivasToolStripMenuItem
        '
        Me.EnseñanzaDeHabilidadesProductivasToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.EnseñanzaDeHabilidadesProductivasToolStripMenuItem.Name = "EnseñanzaDeHabilidadesProductivasToolStripMenuItem"
        resources.ApplyResources(Me.EnseñanzaDeHabilidadesProductivasToolStripMenuItem, "EnseñanzaDeHabilidadesProductivasToolStripMenuItem")
        '
        'EnseñanzaDeHabilidadesReceptivasToolStripMenuItem
        '
        Me.EnseñanzaDeHabilidadesReceptivasToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.EnseñanzaDeHabilidadesReceptivasToolStripMenuItem.Name = "EnseñanzaDeHabilidadesReceptivasToolStripMenuItem"
        resources.ApplyResources(Me.EnseñanzaDeHabilidadesReceptivasToolStripMenuItem, "EnseñanzaDeHabilidadesReceptivasToolStripMenuItem")
        '
        'EstrategiasDeEnseñanzaEnLaLenguaInglesaIIToolStripMenuItem
        '
        Me.EstrategiasDeEnseñanzaEnLaLenguaInglesaIIToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.EstrategiasDeEnseñanzaEnLaLenguaInglesaIIToolStripMenuItem.Name = "EstrategiasDeEnseñanzaEnLaLenguaInglesaIIToolStripMenuItem"
        resources.ApplyResources(Me.EstrategiasDeEnseñanzaEnLaLenguaInglesaIIToolStripMenuItem, "EstrategiasDeEnseñanzaEnLaLenguaInglesaIIToolStripMenuItem")
        '
        'EstructuraGramáticalToolStripMenuItem
        '
        Me.EstructuraGramáticalToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.EstructuraGramáticalToolStripMenuItem.Name = "EstructuraGramáticalToolStripMenuItem"
        resources.ApplyResources(Me.EstructuraGramáticalToolStripMenuItem, "EstructuraGramáticalToolStripMenuItem")
        '
        'InglesVToolStripMenuItem
        '
        Me.InglesVToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.InglesVToolStripMenuItem.Name = "InglesVToolStripMenuItem"
        resources.ApplyResources(Me.InglesVToolStripMenuItem, "InglesVToolStripMenuItem")
        '
        'IntegradoraIIToolStripMenuItem
        '
        Me.IntegradoraIIToolStripMenuItem.ForeColor = System.Drawing.Color.DarkCyan
        Me.IntegradoraIIToolStripMenuItem.Name = "IntegradoraIIToolStripMenuItem"
        resources.ApplyResources(Me.IntegradoraIIToolStripMenuItem, "IntegradoraIIToolStripMenuItem")
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(110, Byte), Integer))
        Me.Label4.Name = "Label4"
        '
        'NESI
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(148, Byte), Integer))
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.Name = "NESI"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Public WithEvents ErCuatrimestreToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ErCuatrimestreToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DesarrolloHumanoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExpresiónOralYEscritaIToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FormaciónSocioculturalIToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FundamentosPedagógicosEnLaEducaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InformáticaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InglésIToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MatemáticasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DoCuatrimestreToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DiseñoDeMaterialDidácticoIToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EstadísticaAplicadaALaEducaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExpresiónOralYEscritaIIToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InglésIIToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MetodologíaDeLaDidácticaIToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MetodologíaDeLaInvestigaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RoCuatrimestreToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EvaluaciónDelProcesoEnseñanzaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FormaciónSocioculturalIIIToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InglésIIIToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaEducaciónEnMéxicoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MetodologíaDeLaDidácticaIIToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MetodologíaDeLaInvestigaciónToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlaneaciónDocenteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToCuatrimestreToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DiseñoDeSituacionesDeEnseñanzaYAprendizajeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EstrategiasDeEnseñanzaEnLaEducaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExpresiónOralYEscritaIIToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FonéticaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FormaciónSocioculturalIVToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InstrumentosDeEvaluaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IntegradoraIToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlaneaciónDocenteToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToCuatrmestreToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContabilidadDeCostosIIToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EnseñanzaDeHabilidadesProductivasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EnseñanzaDeHabilidadesReceptivasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EstrategiasDeEnseñanzaEnLaLenguaInglesaIIToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EstructuraGramáticalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InglesVToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IntegradoraIIToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label4 As System.Windows.Forms.Label

End Class
