﻿Public Class EoyeI
    Private Sub Label2_Click_1(sender As Object, e As EventArgs) Handles Label2.Click
        OpenFileDialog1.Filter = "PDF FILES | *.pdf"


        Dim DesarrolloHumano As String
        DesarrolloHumano = "C:\Users\Patty\Desktop\Material de ayuda\Lengua Inglesa\1° Cuatrimestre\EXPRESIÓN ORAL Y ESCRITA I\Manual de Géneros - Alegría Margarita.pdf"

        If (System.IO.File.Exists(DesarrolloHumano)) Then
            OpenFileDialog1.FileName = DesarrolloHumano
            AxAcroPDF1.src = OpenFileDialog1.FileName
        Else

            Dim Respuesta As DialogResult
            Respuesta = MessageBox.Show("El documento no existe. Favor de contactar al admninistrador." &
                            Environment.NewLine & "¿Deseas cargar el documento manualmente?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

            If (Respuesta = Windows.Forms.DialogResult.Yes) Then
                If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                    AxAcroPDF1.src = OpenFileDialog1.FileName
                End If
            Else
                MessageBox.Show("No se selecciono documento", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        End If
    End Sub

    'Segundo Libro'
    Private Sub Label3_Click_1(sender As Object, e As EventArgs) Handles Label3.Click
        OpenFileDialog1.Filter = "PDF FILES | *.pdf"

        Dim DesarrolloHumano As String
        DesarrolloHumano = "C:\Users\Patty\Desktop\Material de ayuda\Lengua Inglesa\1° Cuatrimestre\EXPRESIÓN ORAL Y ESCRITA I\Redacción desde Cuestiones Gramaticales hasta el Informe Final Extenso - Saad Antonio.PDF"

        If (System.IO.File.Exists(DesarrolloHumano)) Then
            OpenFileDialog1.FileName = DesarrolloHumano
            AxAcroPDF1.src = OpenFileDialog1.FileName
        Else

            Dim Respuesta As DialogResult
            Respuesta = MessageBox.Show("El documento no existe. Favor de contactar al admninistrador." &
                            Environment.NewLine & "¿Deseas cargar el documento manualmente?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

            If (Respuesta = Windows.Forms.DialogResult.Yes) Then
                If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                    AxAcroPDF1.src = OpenFileDialog1.FileName
                End If
            Else
                MessageBox.Show("No se selecciono documento", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub
End Class