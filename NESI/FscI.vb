﻿Public Class FscI

    Private Sub FscI_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs) Handles Label2.Click
        OpenFileDialog1.Filter = "PDF FILES | *.pdf"


        Dim DesarrolloHumano As String
        DesarrolloHumano = "C:\Users\Patty\Desktop\Material de ayuda\Lengua Inglesa\1° Cuatrimestre\FORMACIÓN SOCIOCULTURAL I\Desarrollo del potencial humano Vol.1 - Lafarga, J. y Gómez del Campo, J..pdf"

        If (System.IO.File.Exists(DesarrolloHumano)) Then
            OpenFileDialog1.FileName = DesarrolloHumano
            AxAcroPDF1.src = OpenFileDialog1.FileName
        Else

            Dim Respuesta As DialogResult
            Respuesta = MessageBox.Show("El documento no existe. Favor de contactar al admninistrador." &
                            Environment.NewLine & "¿Deseas cargar el documento manualmente?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

            If (Respuesta = Windows.Forms.DialogResult.Yes) Then
                If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                    AxAcroPDF1.src = OpenFileDialog1.FileName
                End If
            Else
                MessageBox.Show("No se selecciono documento", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        End If
    End Sub

    Private Sub Label4_Click(sender As Object, e As EventArgs) Handles Label4.Click
        OpenFileDialog1.Filter = "PDF FILES | *.pdf"


        Dim DesarrolloHumano As String
        DesarrolloHumano = "C:\Users\Patty\Desktop\Material de ayuda\Lengua Inglesa\1° Cuatrimestre\FORMACIÓN SOCIOCULTURAL I\Introducción al pensamiento sistémico - O'Connor, J.pdf"

        If (System.IO.File.Exists(DesarrolloHumano)) Then
            OpenFileDialog1.FileName = DesarrolloHumano
            AxAcroPDF1.src = OpenFileDialog1.FileName
        Else

            Dim Respuesta As DialogResult
            Respuesta = MessageBox.Show("El documento no existe. Favor de contactar al admninistrador." &
                            Environment.NewLine & "¿Deseas cargar el documento manualmente?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

            If (Respuesta = Windows.Forms.DialogResult.Yes) Then
                If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                    AxAcroPDF1.src = OpenFileDialog1.FileName
                End If
            Else
                MessageBox.Show("No se selecciono documento", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        End If
    End Sub
End Class